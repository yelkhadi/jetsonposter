\documentclass[a0,portrait]{a0poster}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{mathtools}
\usepackage{wrapfig} % Allows wrapping text around tables and figures
\usepackage[hyphens]{url}
\usepackage{sectsty}
\sectionfont{\normalfont\Large\itshape\underline}
% \subsectionfont{\normalfont\underline}
% \usepackage{tcolorbox}
% \newtcolorbox{mybox}[1]{colback=gray!1!white,colframe=gray!75!white,fonttitle=\bfseries,title=#1}

\begin{document}

%----------------------------------------------------------------------------------------
%	POSTER HEADER
%----------------------------------------------------------------------------------------

% The header is divided into two boxes:
% The first is 75% wide and houses the title, subtitle, names, university/organization and contact information
% The second is 25% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit

\begin{minipage}[b]{0.75\linewidth}
\Huge \color{NavyBlue} \textbf{Une approche bayésienne pour la reconnaissance des périodes de sommeil à l’aide de capteurs de mouvements} \color{Black}\\[1.5cm] % Title
% \Huge\textit{An Exploration of Complexity}\\[2cm] % Subtitle
\large \textbf{Yassine El-Khadiri\textsuperscript{1, 2} \& Gabriel Corona\textsuperscript{1} \& Cédric Rose\textsuperscript{1} \& François Charpillet\textsuperscript{2}}\\[0.5cm] % Author(s)
\large \textsuperscript{1}Diatélic, Pharmagest --- \texttt{prénom.nom@diatelic.fr}\\[0.4cm] % University/organization
\large \textsuperscript{2}Université de Lorraine, CNRS, Inria, LORIA F-54000 Nancy, France --- \textit{prénom.nom@inria.fr}
\end{minipage}
%
\begin{minipage}[b]{0.25\linewidth}
\includegraphics[width=20cm]{logo_poster}\\
\end{minipage}

\vspace{3cm} % A bit of extra whitespace between the header and poster content

%----------------------------------------------------------------------------------------

\begin{multicols}{2} % This is how many columns your poster will be broken into, a portrait poster is generally split into 2 columns

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\color{Navy} % Navy color for the abstract
\Large
% \begin{abstract}
\section*{Résumé}

Le vieillissement de la population confronte les sociétés modernes à une transformation démographique sans précédent qui ne va pas sans poser de nombreux problèmes. Outre les aspects économiques, le placement des personnes âgées n'est bien souvent qu'un choix de raison et peut être assez mal vécu par les personnes.
Une réponse à cette problématique sociétale est le développement des technologies qui facilitent le maintien à domicile des personnes âgées. Nous nous intéressons au problème particulier du suivi de la qualité du sommeil ainsi qu'à la détection des levés nocturnes d'une personne vivant seule à son domicile. Nous présenterons une méthode d'inférence bayésienne  qui permet à notre solution  d'être assez flexible et robuste aux différents types d'installation et configuration d'appartements tout en maintenant une précision de prédiction de 0.94. Cette solution est en cours de déploiement sur plusieurs dizaines d'appartements en Lorraine.

% \end{abstract}
\normalsize

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\color{SaddleBrown} % SaddleBrown color for the introduction

% \section*{Introduction}
%
% La part des personnes âgées de plus de 60 ans dans la population mondiale augmente et devrait atteindre les 17\%
% d'ici 2050.
% Leur prise en charge des plus âgés devient de plus en plus difficile compte tenu du contexte socio-économique et du déséquilibre démographique.
% D'après la base de donnée de la Banque mondiale,
% le ratio de dépendance des personnes âgées, qui est le rapport entre
% le nombre de personnes âgées de plus de 64 ans et la population en âge de travailler
% est passée de 8,6\% en 1960 à 12,9\% en 2016 \cite{age-dependency-ratio}.
% Ce travail s'inscrit dans le cadre d'un projet “36 mois de plus” d'élaboration d'une solution d'assistance à domicile aux personnes âgées dont le but essentiel est de les maintenir à domicile plus longtemps qu'elles ne le peuvent aujourd'hui.

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------

\color{DarkSlateGray} % DarkSlateGray color for the rest of the content

\section*{Objectifs}

\begin{enumerate}
\item Élaboration d'une solution d'assistance à domicile aux personnes âgées
\item Solution reposant sur le placement de capteurs ambiants à domicile
\item Dans le cadre de ce travail des détecteurs de mouvement
\item Dont les données sont ensuite interprétées pour l'inférence des périodes de sommeil
\end{enumerate}

%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------

\section*{Approche}

\subsection*{1) Données:}

% \begin{mybox}{Données}

\begin{center}
\includegraphics[width=0.6\linewidth]{nights}
\captionof{figure}{\color{Green} Illustration de données de capteurs centrées autour de minuit montrant un clair changement dans le nombre d'activation}
\end{center}

\begin{itemize}
  \item Séries temporelles provenant de capteurs placés dans chaque chambre du logement
  \item Données centrées vers minuit et ré-échantillonnées sur des tranches de 10 minutes
\end{itemize}

% \end{mybox}

\subsection*{2) Modèle:}

% \begin{mybox}{Modèle}

\begin{center}
\includegraphics[width=0.5\linewidth]{multi_graphical_model}
\captionof{figure}{\color{Green} Représentation graphique du réseau bayésien du modèle de sommeil}
\end{center}

\begin{itemize}
\item Le modèle que nous proposons est fondé sur le \textit{Bayesian Switch-point Model},
méthode aussi connue sous le nom de \textit{Bayesian Change Point Analysis}\cite{changepoint}
\item \begin{equation}
	M_t \sim \mathcal{B}(P_t),~~
	P_t =
	\left\lbrace
	\begin{matrix}
	P_s & \mathrm{if}~ S_t = 0 \\
	P_w & \mathrm{if}~ S_t = 1
	\end{matrix}
	\right.
\label{equ:M_bernoulli}
\end{equation}

\begin{equation}
	S_t =
	\left\lbrace
	\begin{array}{lll}
	0 & \mathrm{if}~ & T_s \le t < T_w\\
	1 & \mathrm{if}~ & t < T_s ~\mathrm{or}~ t \ge T_w
	\end{array}
	\right.
\label{equ:S_t_def}
\end{equation}
\end{itemize}

% \end{mybox}

\subsection*{3) Algorithme:}

% \begin{mybox}{Algorithme}

\begin{itemize}
\item Estimation du Maximum A Posteriori:
\begin{equation}
T_s,~T_w =
\underset{T_s, T_w}{\mathrm{argmax} ~}
\max_{P_s, P_w}
P(T_s, T_w, P_s, P_w ~|~ M)
\label{equ:map}
\end{equation}
avec :
\begin{equation}
% \begin{split}
 P(T_s, T_w, P_s, P_w ~|~ M) \propto P(M ~|~ T_s, T_w, P_s, P_w) \times P(T_w) \times P(D)
% \end{split}
\label{equ:map_detail}
\end{equation}
où
\begin{equation}
D = T_w - T_s
\end{equation}
i.e.
\begin{equation}
% \begin{split}
\underset{T_s, T_w}{\mathrm{argmax} ~}
\prod_{T_s \leq t < T_w} p \left( M_t ~\middle|~ P_s = \frac{\sum_{T_s \leq t < T_w} M_t}{\text{Card}([T_s, T_w])} \right)
\times
\prod_{\underset{T_w \leq t < N}{0 \leq t < T_s}} p \left(M_t ~\middle|~ P_w = \frac{\sum_{\underset{T_w \leq t < N}{0 \leq t < T_s}} M_t}{\text{Card}([0, T_s[ \cup [T_w, N[)} \right)
% \end{split}
\label{equ:no-soft-evidence}
\end{equation}
\item \textit{Soft evidence}:
On considère que les observations $M$ ne sont plus déterministes mais suivent une \textit{"distribution"} $e(M_t ~|~ X_t)$\cite{Pearl88}. On dit que $e$ est un processus externe utilisé dans le calcul pour modéliser l'incertitude des observations dont les valeurs sont définies comme ceci:
\begin{center}
\begin{tabular}{c|c|c}
$e$       & $M_t = 0$ & $M_t = 1$\\\hline
$X_t = 0$ & 0.3       & 0.7      \\\hline
$X_t = 1$ & 0         & 1
\end{tabular}
\end{center}

Ainsi la nouvelle fonction objectif de l'optimisation est la log-vraissemblence:
\begin{equation}
\int_{M \sim e(X)}
\log \left(
P(T_w) \times P(D) \times \mathcal{L}
\right)
\mathrm{d} e(X)
\end{equation}
% qui se calcule comme ceci:
% \begin{equation}
% % \begin{split}
% \underset{T_s, T_w}{\mathrm{argmax} ~} \sum_{s = \{0, 1\}} \sum_{S_t = s} \sum_{x = \{0, 1\}}
% \left( \log \left( e(X_t=x|M_t) \times P(M_t|\hat{P}_t) \right) \delta(M_t=x) \right)
% % \end{split}
% \label{equ:soft-evidence}
% \end{equation}
% avec:
% \begin{equation}
% % \begin{split}
% \hat{P}_s = \frac{\sum_{t | S_t=0} E[M_t]} {\mathrm{Card}(\{t | S_t = 0\})}
%  = \frac{
% \splitfrac{
% \sum_{\{t|S_t=0\}} e(X_t = 0 ~|~ M_t = 1) \delta(M_t = 0)
% }{
% + e(X_t = 1 ~|~ M_t = 1) \delta(M_t = 1)
% }
% }
% {\mathrm{Card}(\{t | S_t = 0\})}
% % \end{split}
% \end{equation}
% sachant que:
% \begin{equation}
% \delta(A = x) = \begin{cases}
%   1 & \text{si} ~ A = x\\
%   0 & \text{sinon}
% \end{cases}
% \end{equation}
\end{itemize}

% \end{mybox}

%----------------------------------------------------------------------------------------
%	RESULTS
%----------------------------------------------------------------------------------------

\section*{Résultats}

\begin{center}
\includegraphics[width=0.6\linewidth]{error_distribution}
\captionof{figure}{\color{Green} Diagramme en boite de l'erreur sur $T_s$ et $T_w$}
\end{center}

\begin{center}
\includegraphics[width=0.6\linewidth]{accuracy_precision_recall_f1_scores}
\captionof{figure}{\color{Green} Histogramme de la justesse, précision, rappel et score F1 des nuits}
\end{center}

%----------------------------------------------------------------------------------------
%	CONCLUSIONS
%----------------------------------------------------------------------------------------

\color{SaddleBrown} % SaddleBrown color for the conclusions to make them stand out

\section*{Conclusions}

\begin{itemize}
\item Méthode non supervisée simple et peu coûteuse en calcul de détection des périodes de sommeil à l'aide de séries temporelles binaires de données actigraphiques
\item Facilement intégrable à des dispositifs de surveillance médicale existants
\item Prend également en charge un nombre de capteurs qui peut changer dynamiquement
\item Actuellement en production en phase d'expérimentation du projet
\end{itemize}

\color{DarkSlateGray} % Set the color back to DarkSlateGray for the rest of the content

%----------------------------------------------------------------------------------------
%	FORTHCOMING RESEARCH
%----------------------------------------------------------------------------------------

\section*{Axes de recherche}

\begin{itemize}
\item Évaluation de la confiance sur le résultat de l'inférence
\item Score de sommeil suivant plusieurs critères (durée, temps, levées nocturnes)
\item Détection de sommeil segmenté
\end{itemize}

 %----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

\nocite{*} % Print all references regardless of whether they were cited in the poster or not
\bibliographystyle{plain} % Plain referencing style
\bibliography{biblio} % Use the example bibliography file sample.bib

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------

\end{multicols}
\end{document}
